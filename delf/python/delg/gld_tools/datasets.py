import os
import pandas as pd


_DATASET_ROOT = "/media/LTDataset/google-landmark"

def getImgpathlist(_DATASET_ROOT, _DATASET_TYPE):
    image_root = os.path.join(_DATASET_ROOT, _DATASET_TYPE)
    train_df = pd.read_csv(os.path.join(_DATASET_ROOT, "train.csv"))
    id_list = train_df['id'].tolist()
    landmarkid_list = train_df['landmark_id'].tolist()
    image_list = [os.path.join(image_root, f[0], f[1], f[2] , str(f) +".jpg")  for f in id_list]
    return image_list, landmarkid_list

if __name__=="__main__":
    image_list, landmarkid_list = getImgpathlist(_DATASET_ROOT, 'train')
    print(image_list[0])
    print(landmarkid_list[0])
