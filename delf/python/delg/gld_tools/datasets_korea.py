import os
import pandas as pd

IMG_ROOT = "/media/TrainDataset/dataset/Google_landmark/images"
INCLUDE_NORTH = True
INCLUDE_SOUTH = True


def get_img_paths(img_type):
    img_dir_list = []
    img_path_list = []
    img_label_list = []
    if INCLUDE_NORTH:
        img_dir_list.append(os.path.join(IMG_ROOT, img_type, 'north'))

    if INCLUDE_SOUTH:
        img_dir_list.append(os.path.join(IMG_ROOT, img_type, 'south'))

    for img_dir in img_dir_list:
        for r, d, f in os.walk(img_dir):
            for filename in f:
                if '.jpg' in filename:
                    img_label_list.append(r.split("/")[-1])
                    img_path_list.append(os.path.join(r, filename))


    return img_path_list,  img_label_list



if __name__=="__main__":
    img_path_list, img_label_list = _get_img_paths('test')
    print(img_path_list)
