# Lint as: python3
# Copyright 2020 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Performs DELG-based image retrieval on Revisited Oxford/Paris datasets."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import time

from absl import app
from absl import flags
import numpy as np

from delf import datum_io
from delf.python.detect_to_retrieve import dataset
from delf.python.detect_to_retrieve import image_reranking

from gld_tools.datasets_korea import get_img_paths

import pdb
import cv2
import pickle

FLAGS = flags.FLAGS

flags.DEFINE_string(
    'dataset_file_path', './data/gnd_roxford5k.mat',
    'Dataset file for Revisited Oxford or Paris dataset, in .mat format.')
flags.DEFINE_string('query_features_dir', './data/oxford5k_features/query',
                    'Directory where query DELG features are located.')
flags.DEFINE_string('index_features_dir', './data/oxford5k_features/index',
                    'Directory where index DELG features are located.')
flags.DEFINE_boolean(
    'use_geometric_verification', True,
    'If True, performs re-ranking using local feature-based geometric '
    'verification.')
flags.DEFINE_float(
    'local_descriptor_matching_threshold', 1.0,
    'Optional, only used if `use_geometric_verification` is True. '
    'Threshold below which a pair of local descriptors is considered '
    'a potential match, and will be fed into RANSAC.')
flags.DEFINE_float(
    'ransac_residual_threshold', 20.0,
    'Optional, only used if `use_geometric_verification` is True. '
    'Residual error threshold for considering matches as inliers, used in '
    'RANSAC algorithm.')
flags.DEFINE_boolean(
    'use_ratio_test', False,
    'Optional, only used if `use_geometric_verification` is True. '
    'Whether to use ratio test for local feature matching.')
flags.DEFINE_string(
    'output_dir', '/data/oxford5k_features/retrieval',
    'Directory where retrieval output will be written to. A file containing '
    "metrics for this run is saved therein, with file name 'metrics.txt'.")
flags.DEFINE_boolean(
    'save_ranks', False, 
    'whether to save the gv-calculated rank')
flags.DEFINE_string(
    'rank_save_dir', '/media/TrainDataset/dataset/Google_landmark/rank_files/delg_rank.pickle',
    'Path of rank file to be saved')
flags.DEFINE_string(
    'rank_load_dir', '/media/TrainDataset/dataset/Google_landmark/rank_files/delg_rank.pickle',
    'Load the directory of saved rank')
flags.DEFINE_boolean(
    'save_visualization', True,
    'If true, save the inferenced images')
flags.DEFINE_boolean(
    'use_saved_ranks_for_gv', True, 'whether to condoct gv or import saved result')

# Extensions.
_DELG_GLOBAL_EXTENSION = '.delg_global'
_DELG_LOCAL_EXTENSION = '.delg_local'

# Precision-recall ranks to use in metric computation.
_PR_RANKS = (1, 5, 10)

# Pace to log.
_STATUS_CHECK_LOAD_ITERATIONS = 50

# Output file names.
_METRICS_FILENAME = 'metrics.txt'


def _ReadDelgDescriptors(image_list, image_type, feature_extension):
  """Reads DELG global features.

  Args:
    input_dir: Directory where features are located.
    image_list: List of image names for which to load features.

  Returns:
    global_descriptors: NumPy array of shape (len(image_list), D), where D
      corresponds to the global descriptor dimensionality.
  """
  num_images = len(image_list)
  global_descriptors = []
  descriptor_root = os.path.join('/media/TrainDataset/dataset/Google_landmark', 'features', 'delg_features', image_type)
  print('Starting to collect global descriptors for %d images...' % num_images)
  start = time.time()
  for i in range(num_images):
    if i > 0 and i % _STATUS_CHECK_LOAD_ITERATIONS == 0:
      elapsed = (time.time() - start)
      print('Reading global descriptors for image %d out of %d, last %d '
            'images took %f seconds' %
            (i, num_images, _STATUS_CHECK_LOAD_ITERATIONS, elapsed))
      start = time.time()

    descriptor_name = image_list[i].split("/")[-1][:-4] + feature_extension
    descriptor_fullpath = os.path.join(descriptor_root, descriptor_name)
    if feature_extension == '.delg_local':
        global_descriptors.append(descriptor_fullpath)
    else:
        global_descriptors.append(datum_io.ReadFromFile(descriptor_fullpath))

  if feature_extension == '.delg_local':
      return global_descriptors
  return np.array(global_descriptors)


def visualize_results(ranks, query_image, query_label, index_list, index_label):
    is_in_count = 0
    n_value = 29
    img_q = cv2.resize(cv2.imread(query_image), dsize=(360, 480))
    cv2.rectangle(img_q, (0, 0), (360, 480), (255, 0, 0), 10)
    img_np_list = [img_q]
    for k, idx in enumerate(ranks[:28]): #0, 8187
        app_img = cv2.resize(cv2.imread(index_list[idx]), dsize=(360, 480))
        if index_label[idx] == query_label:
            is_in_count += 1
            cv2.rectangle(app_img, (0, 0), (360, 480), (0, 255, 0), 10)
        img_np_list.append(app_img)
    print("IS IN ", is_in_count, " times!")
    # Divide 30 images into 5 by 6
    row_len = 5
    col_len = (n_value+1) // row_len

    img_hconcat_list=[]
    for j in range(0, n_value - col_len, col_len):
        img_hconcat_list.append(np.concatenate((img_np_list[j:j+col_len]), axis=1))

    img_concat = np.concatenate((img_hconcat_list), axis=0)
    cv2.imwrite('/media/TrainDataset/dataset/Google_landmark/inferenced_images/delg_inferences/query_' + query_image.split("/")[-1], img_concat)
    return 0


def perform_recognition_metric(tolerances, ranks, query_labels, index_labels):
    log_list=[]
    for tolerance in tolerances:
        print ("Performing retrieval accuracy in tolerance #", tolerance)
        in_tolerance_count = 0
        for i, query_label in enumerate(query_labels):
            flag = False
            for idx in ranks[i, :tolerance]:
                if index_labels[idx] == query_label:
                    flag = True
            if flag:
                print("Retrieved!")
                in_tolerance_count += 1
        print("@", tolerance, " precision: ", 
              in_tolerance_count/len(query_labels))
        log_list.append((tolerance, in_tolerance_count/len(query_labels)))
    return log_list


def main(argv):
    if len(argv) > 1:
        raise RuntimeError('Too many command-line arguments.')

    # Parse dataset to obtain query/index images, and ground-truth.
    print('Parsing dataset...')
    query_list, query_label = get_img_paths('test')
    index_list, index_label = get_img_paths('train')   
    num_query_images = len(query_list)
    num_index_images = len(index_list)
    print('done! Found %d queries and %d index images' %
         (num_query_images, num_index_images))

    
    if not FLAGS.use_saved_ranks_for_gv:
      
        # Read global/local features.
        query_global_features = _ReadDelgDescriptors(query_list, 'test', _DELG_GLOBAL_EXTENSION)
        index_global_features = _ReadDelgDescriptors(index_list, 'train', _DELG_GLOBAL_EXTENSION)
        query_local_features = _ReadDelgDescriptors(query_list, 'test', _DELG_LOCAL_EXTENSION)
        index_local_features = _ReadDelgDescriptors(index_list, 'train', _DELG_LOCAL_EXTENSION)
        
        # Compute similarity between query and index images, potentially re-ranking
        # with geometric verification.
        ranks_before_gv = np.zeros([num_query_images, num_index_images],
                                   dtype='int32')
        if FLAGS.use_geometric_verification:
            ranks_after_gv = np.zeros([num_query_images, num_index_images],
                                         dtype='int32')
        for i in range(num_query_images):
            print('Performing retrieval with query %d (%s)...' % (i, query_list[i]))
            start = time.time()

            # Compute similarity between global descriptors.
            similarities = np.dot(index_global_features, query_global_features[i])
            ranks_before_gv[i] = np.argsort(-similarities)

            # Re-rank using geometric verification.
            if FLAGS.use_geometric_verification:
                ranks_after_gv[i] = image_reranking.GLDKRRerankByGeometricVerification(
                                                    input_ranks=ranks_before_gv[i],
                                                    initial_scores=similarities,
                                                    query_name=query_local_features[i],
                                                    index_names=index_local_features,
                                                    local_feature_extension=_DELG_LOCAL_EXTENSION,
                                                    ransac_seed=0,
                                                    descriptor_matching_threshold=FLAGS
                                                    .local_descriptor_matching_threshold,
                                                    ransac_residual_threshold=FLAGS.ransac_residual_threshold,
                                                    use_ratio_test=FLAGS.use_ratio_test)
            elapsed = (time.time() - start)
            pdb.set_trace()
            print('done! Retrieval for query %d took %f seconds' % (i, elapsed))

        if FLAGS.save_ranks:
            with open(FLAGS.rank_save_dir, 'wb') as f:
                pickle.dump(ranks_after_gv, f)
            print("Rank pickle file saved!")

    else:
        with open(FLAGS.rank_load_dir, 'rb') as f:
            ranks_after_gv = pickle.load(f)
        print("rank shape: ", np.asarray(ranks_after_gv).shape)

    if FLAGS.save_visualization:
        for i in range(num_query_images):
            visualize_results(ranks_after_gv[i], query_list[i], query_label[i], index_list, index_label)
    
    
    result =  perform_recognition_metric([1, 3, 5, 10, 15, 20, 25, 30], ranks_after_gv, query_label, index_label)
    print("result: ", result)
    

if __name__ == '__main__':
    app.run(main)
